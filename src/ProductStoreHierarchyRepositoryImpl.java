package com.ga.endeavour.productstore.repository;

import com.ga.endeavour.platform.payload.hierarchy.request.HierarchyEntityRequest;
import com.ga.endeavour.platform.payload.hierarchy.request.HierarchyNodeRequest;
import com.ga.endeavour.productstore.dto.EntityDTO;
import com.ga.endeavour.productstore.dto.HierarchyDTO;
import com.ga.endeavour.productstore.dto.NodeDTO;
import com.ga.endeavour.productstore.util.ProductStoreHierarchyUtil;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository("productStoreHierarchyRepository")
public class ProductStoreHierarchyRepositoryImpl implements ProductStoreHierarchyRepository {

    private static final String PROGRAM_CODE = "programCode";
    private static final String HIERARCHY_TYPE_ID = "hierarchyTypeId";
    public static final String HIERARCHY_DESCRIPTION = "hierarchyDescription";
    public static final String HIERARCHY_NAME = "hierarchyName";
    public static final String DATA_PARTNER_CODE = "dataPartnerCode";
    public static final String HIERARCHY_CODE = "hierarchyCode";
    public static final String ID = "id";
    public static final String HIERARCHY_ID = "hierarchyId";

    @PersistenceContext
    private EntityManager entityManager;

    private static ProductStoreHierarchyUtil productStoreHierarchyUtil = new ProductStoreHierarchyUtil();


    @Override
    public int createOrUpdateHierarchy(String programCode, String dataPartnerCode, String hierarchyType,
                                       String hierarchyName, String hierarchyDescription, String hierarchyCode) {

        BigInteger hierarchyTypeId = getHierarchyTypeFromName(hierarchyType);

        HierarchyDTO hierarchyDTO = checkIfHierarchyExists(hierarchyCode, hierarchyTypeId, programCode,
                dataPartnerCode);

        BigInteger id = null;
        if (hierarchyDTO != null && hierarchyDTO.getId() != null) {
            /*
             * this means the hierarchy exists, so we do not want to update or insert. Simply ignore the request
             * */
            return 0;
        } else {

            String queryStr = "insert into ods.psh_product_hierarchy_catalog "
                    + " (id, program_code, datapartner_code, hierarchy_type_id, hierarchy_code, hierarchy_name, "
                    + " hierarchy_description, cdc_created_ts, cdc_updated_ts) values (:id, :programCode,:dataPartnerCode,:hierarchyTypeId, "
                    + ":hierarchyCode ,:hierarchyName, :hierarchyDescription, now(), now())";

            int result = entityManager.createNativeQuery(queryStr).setParameter(ID, id).
                    setParameter(PROGRAM_CODE, programCode).setParameter(DATA_PARTNER_CODE, dataPartnerCode).
                    setParameter(HIERARCHY_TYPE_ID, hierarchyTypeId).setParameter(HIERARCHY_CODE, hierarchyCode).
                    setParameter(HIERARCHY_NAME, hierarchyName).
                    setParameter(HIERARCHY_DESCRIPTION, hierarchyDescription).executeUpdate();

            return result;
        }
    }

    @Override
    public int createOrUpdateNode(HierarchyNodeRequest hierarchyNodeRequest) {
        NodeDTO thisNodeDTO = checkIfNodeExists(hierarchyNodeRequest.getNodeCode(), hierarchyNodeRequest.getProgramCode(),
                hierarchyNodeRequest.getHierarchyCode(), hierarchyNodeRequest.getDataPartnerCode(), hierarchyNodeRequest.getNodeHash());

        String parentNodeHash = "";
        if (hierarchyNodeRequest.getNodeHash() != null && !hierarchyNodeRequest.getNodeHash().isEmpty()) {
            parentNodeHash = hierarchyNodeRequest.getNodeHash().substring(0, (hierarchyNodeRequest.getNodeHash().lastIndexOf(File.separator) + 1));
            if (parentNodeHash != null && !parentNodeHash.equals(File.separator)) {
                parentNodeHash = parentNodeHash.substring(0, parentNodeHash.length() - 1);
            }
        }
        NodeDTO parentNodeDTO = getParentNode(hierarchyNodeRequest.getParentNodeCode(), hierarchyNodeRequest.getProgramCode(),
                hierarchyNodeRequest.getHierarchyCode(), hierarchyNodeRequest.getDataPartnerCode(), parentNodeHash);

        BigInteger thisNodeLevel = BigInteger.ZERO;
        BigInteger parentId = BigInteger.ZERO;
        if (parentNodeDTO != null && parentNodeDTO.getId() != null) {
            //this is not the root node
            thisNodeLevel = parentNodeDTO.getNodeLevel().add(BigInteger.ONE);
            parentId = parentNodeDTO.getId();
        }

        BigInteger id = null;
        if (thisNodeDTO != null && thisNodeDTO.getId() != null) {
            id = thisNodeDTO.getId();
        } else {
            thisNodeDTO = new NodeDTO();
        }

        HierarchyDTO hierarchyDTO = getHierarchyDTOFromHierarchyCode(hierarchyNodeRequest.getHierarchyCode(),
                hierarchyNodeRequest.getDataPartnerCode(), hierarchyNodeRequest.getProgramCode());
        if (hierarchyDTO != null && hierarchyDTO.getId() != null) {
            thisNodeDTO.setHierarchyId(hierarchyDTO.getId());
        }
        List<NodeDTO> nodeDTOList = new ArrayList<>();
        if (parentNodeDTO != null) {
            nodeDTOList = getAllChildNodesForThisNode(parentNodeDTO);
        }
        thisNodeDTO = productStoreHierarchyUtil.calculateNodeValues(thisNodeDTO, parentNodeDTO, nodeDTOList);
        String queryStr;
        if (id == null) {
            queryStr = "insert into ods.psh_product_hierarchy_node (id, node_code, node_name, node_hash, parent_id, "
                    + " node_level, node_left_value, node_right_value, hierarchy_id) "
                    + " values (:id, :nodeCode, :nodeName, :nodeHash, :parentId, :nodeLevel, "
                    + " :nodeLeftValue, :nodeRightValue, :hierarchyId)";
        } else {
            queryStr = "update  ods.psh_product_hierarchy_node set node_code = :nodeCode, node_name = :nodeName, node_hash = :nodeHash, "
                    + " parent_id = :parentId, node_level = :nodeLevel, node_left_value = :nodeLeftValue, node_right_value = :nodeRightValue,"
                    + " hierarchy_id = :hierarchyId where id = :id";
        }
        int result = entityManager.createNativeQuery(queryStr).setParameter("id", id).
                setParameter("nodeCode", hierarchyNodeRequest.getNodeCode()).
                setParameter("nodeName", hierarchyNodeRequest.getNodeName()).
                setParameter("nodeHash", hierarchyNodeRequest.getNodeHash()).
                setParameter("parentId", parentId).setParameter("nodeLevel", thisNodeLevel).
                setParameter("nodeLeftValue", thisNodeDTO.getNodeLeftValue()).
                setParameter("nodeRightValue", thisNodeDTO.getNodeRightValue()).
                setParameter("hierarchyId", thisNodeDTO.getHierarchyId()).executeUpdate();
        return result;
    }

    @Override
    public int createOrUpdateEntity(HierarchyEntityRequest hierarchyEntityRequest) {

        EntityDTO entityDTO = checkIfEntityExists(hierarchyEntityRequest.getEntityCode(),
                hierarchyEntityRequest.getHierarchyCode(), hierarchyEntityRequest.getParentHash(),
                hierarchyEntityRequest.getProgramCode(), hierarchyEntityRequest.getDataPartnerCode());

        NodeDTO parentNodeDTO = checkIfNodeExists(hierarchyEntityRequest.getParentNodeCode(),
                hierarchyEntityRequest.getProgramCode(), hierarchyEntityRequest.getHierarchyCode(),
                hierarchyEntityRequest.getDataPartnerCode(), hierarchyEntityRequest.getParentHash());

        BigInteger parentNodeId = BigInteger.ZERO;
        if (parentNodeDTO != null && parentNodeDTO.getId() != null) {
            parentNodeId = parentNodeDTO.getId();
        }

        BigInteger id = null;
        if (entityDTO != null && entityDTO.getId() != null) {
            id = entityDTO.getId();
        }

        String queryStr = "replace into ods.psh_product_hierarchy_entity (id, entity_code, node_id, entity_name, "
                + " cdc_created_ts, cdc_updated_ts) values (:id, :entityCode, :nodeId, :entityName, now(), now())";

        int result = entityManager.createNativeQuery(queryStr).setParameter("id", id).
                setParameter("entityCode", hierarchyEntityRequest.getEntityCode()).
                setParameter("nodeId", parentNodeId).
                setParameter("entityName", hierarchyEntityRequest.getEntityName()).executeUpdate();

        return result;
    }

    private List<NodeDTO> getAllChildNodesForThisNode(NodeDTO thisNodeDTO) {

        List<NodeDTO> nodeDTOList = new ArrayList<>();
        String queryStr = "select id, node_code, node_name, parent_id, node_level,node_left_value, node_right_value,"
                + " hierarchy_id, node_hash from ods.psh_product_hierarchy_node where hierarchy_id = :hierarchyId"
                + " and parent_id = :parentId";

        List<Object[]> childrenNodeList = entityManager.createNativeQuery(queryStr).
                setParameter(HIERARCHY_ID, thisNodeDTO.getHierarchyId()).
                setParameter("parentId", thisNodeDTO.getId()).getResultList();

        for (Object childNode : childrenNodeList) {
            Object[] tuple = (Object[]) childNode;

            NodeDTO nodeDTO = buildNodeDTOFromResponse(tuple);
            nodeDTOList.add(nodeDTO);
        }
        return nodeDTOList;
    }

    private HierarchyDTO checkIfHierarchyExists(String hierarchyCode, BigInteger hierarchyTypeId, String programCode,
                                                String dataPartnerCode) {

        String queryStr = "select id, program_code, datapartner_code , hierarchy_type_id , hierarchy_name, "
                + " hierarchy_description, hierarchy_code from ods.psh_product_hierarchy_catalog "
                + " where hierarchy_code = :hierarchyCode and hierarchy_type_id = :hierarchyTypeId"
                + " and program_code = :programCode and datapartner_code = :dataPartnerCode";

        List<Object[]> entityResultList = entityManager.createNativeQuery(queryStr).
                setParameter(HIERARCHY_CODE, hierarchyCode).setParameter(HIERARCHY_TYPE_ID, hierarchyTypeId).
                setParameter(PROGRAM_CODE, programCode).setParameter(DATA_PARTNER_CODE, dataPartnerCode).getResultList();

        HierarchyDTO hierarchyDTO = null;
        if (entityResultList != null && !entityResultList.isEmpty()) {
            for (int i = 0; i < entityResultList.size(); i++) {
                Object[] entityResult = entityResultList.get(i);
                Object[] tuple = entityResult;

                hierarchyDTO = buildHierarchyDTOFromResponse(tuple);
            }
        }

        return hierarchyDTO;
    }

    private EntityDTO checkIfEntityExists(String entityCode, String hierarchyCode, String parentHash, String programCode, String dataPartnerCode) {

        String queryStr = "select entity.id, entity_code, node_id, entity_name, node_hash from "
                + "ods.psh_product_hierarchy_entity entity, ods.psh_product_hierarchy_node node, "
                + "ods.psh_product_hierarchy_catalog hierarchy "
                + "where entity.node_id = node.id and hierarchy.id = node.hierarchy_id and entity_code = :entityCode "
                + "and hierarchy_code = :hierarchyCode and program_code = :programCode and datapartner_code = :dataPartnerCode";

        List<Object[]> entityResultList = entityManager.createNativeQuery(queryStr).
                setParameter("entityCode", entityCode).setParameter(HIERARCHY_CODE, hierarchyCode).
                setParameter("programCode", programCode).setParameter("dataPartnerCode", dataPartnerCode).
                getResultList();

        EntityDTO entityDTO = null;
        if (entityResultList != null && !entityResultList.isEmpty()) {
            for (int i = 0; i < entityResultList.size(); i++) {
                Object[] entityResult = entityResultList.get(i);
                Object[] tuple = entityResult;

                entityDTO = buildEntityDTOFromResponse(tuple);
                boolean flag = checkNodeWithHash(entityDTO, parentHash);

                if (flag) {
                    return entityDTO;
                }
            }
        }

        return entityDTO;
    }

    private NodeDTO checkIfNodeExists(String nodeCode, String programCode, String hierarchyCode, String dataPartnerCode, String nodeHash) {

        String queryStr = "select node.id, node_code, node_name, parent_id, node_level,node_left_value, "
                + "node_right_value, hierarchy_id ,node_hash from ods.psh_product_hierarchy_node node , "
                + "ods.psh_product_hierarchy_catalog catalog where hierarchy_id = catalog.id and "
                + "node_code = :nodeCode and program_code = :programCode and hierarchy_code = :hierarchyCode "
                + "and datapartner_code = :dataPartnerCode";

        List<Object[]> nodeResultList = entityManager.createNativeQuery(queryStr).setParameter("nodeCode", nodeCode).
                setParameter(PROGRAM_CODE, programCode).setParameter(HIERARCHY_CODE, hierarchyCode).
                setParameter(DATA_PARTNER_CODE, dataPartnerCode).getResultList();

        NodeDTO nodeDTO = null;
        if (nodeResultList != null && !nodeResultList.isEmpty()) {
            for (int i = 0; i < nodeResultList.size(); i++) {
                Object[] nodeResult = nodeResultList.get(i);
                Object[] tuple = nodeResult;

                nodeDTO = buildNodeDTOFromResponse(tuple);
                boolean flag = checkNodeWithHash(nodeDTO, nodeHash);
                if (flag) {
                    return nodeDTO;
                } else {
                    nodeDTO = null;
                }
            }
        }

        return nodeDTO;
    }

    private BigInteger getHierarchyTypeFromName(String hierarchyName) {

        String queryStr = "select id from ods.psh_product_hierarchy_type where hierarchy_type = :hierarchyTypeName ";

        List<Object> hierarchyResult = entityManager.createNativeQuery(queryStr).setParameter("hierarchyTypeName",
                hierarchyName).getResultList();
        BigInteger result = null;
        if (hierarchyResult != null && hierarchyResult.size() > 0) {
            result = (BigInteger) hierarchyResult.get(0);
        }
        return result;
    }

    private HierarchyDTO getHierarchyDTOFromHierarchyCode(String hierarchyCode, String dataPartnerCode, String programCode) {

        String queryStr = "select id, program_code, datapartner_code, hierarchy_type_id, hierarchy_code, hierarchy_name,"
                + " hierarchy_description from ods.psh_product_hierarchy_catalog where hierarchy_code = :hierarchyCode"
                + " and datapartner_code = :dataPartnerCode and program_code = :programCode";

        List<Object[]> hierarchyResultList = entityManager.createNativeQuery(queryStr).setParameter("hierarchyCode",
                hierarchyCode).setParameter("dataPartnerCode", dataPartnerCode).setParameter("programCode", programCode).getResultList();

        HierarchyDTO hierarchyDTO = null;
        if (hierarchyResultList != null && !hierarchyResultList.isEmpty()) {
            for (int i = 0; i < hierarchyResultList.size(); i++) {
                Object[] hierarchyResult = hierarchyResultList.get(i);
                Object[] tuple = hierarchyResult;

                hierarchyDTO = buildHierarchyDTOFromResponse(tuple);
            }
        }
        return hierarchyDTO;
    }

    private NodeDTO buildNodeDTOFromResponse(Object[] tuple) {

        BigInteger nodeId = (BigInteger) tuple[0];
        String nodeCode = (String) tuple[1];
        String nodeName = (String) tuple[2];
        BigInteger parentId = (BigInteger) tuple[3];
        BigInteger nodeLevel = (BigInteger) tuple[4];
        BigDecimal nodeLeftValue = (BigDecimal) tuple[5];
        BigDecimal nodeRightValue = (BigDecimal) tuple[6];
        BigInteger hierarchyId = (BigInteger) tuple[7];
        String nodeHash = (String) tuple[8];

        return new NodeDTO(nodeId, nodeCode, nodeName, parentId, nodeLevel, nodeLeftValue,
                nodeRightValue, hierarchyId, nodeHash);
    }

    private EntityDTO buildEntityDTOFromResponse(Object[] tuple) {

        BigInteger entityId = (BigInteger) tuple[0];
        String entityCode = (String) tuple[1];
        BigInteger nodeId = (BigInteger) tuple[2];
        String entityName = (String) tuple[3];
        String parentNodeHash = (String) tuple[4];

        return new EntityDTO(entityId, entityCode, nodeId, entityName, parentNodeHash);
    }

    private HierarchyDTO buildHierarchyDTOFromResponse(Object[] tuple) {

        BigInteger hierarchyId = (BigInteger) tuple[0];
        String programCode = (String) tuple[1];
        String dataPartnerCode = (String) tuple[2];
        BigInteger hierarchyTypeId = (BigInteger) tuple[3];
        String hierarchyName = (String) tuple[4];
        String hierarchyDescription = (String) tuple[5];
        String hierarchyCode = (String) tuple[6];

        return new HierarchyDTO(hierarchyId, programCode, dataPartnerCode, hierarchyTypeId,
                hierarchyCode, hierarchyName, hierarchyDescription);
    }

    private boolean checkNodeWithHash(EntityDTO entityDTO, String nodeHash) {
        boolean nodeMatched = false;

        if (entityDTO != null && entityDTO.getParentNodeHash() != null && entityDTO.getParentNodeHash().equalsIgnoreCase(nodeHash)) {
            return true;
        }
        return nodeMatched;
    }

    private boolean checkNodeWithHash(NodeDTO nodeDTO, String nodeHash) {
        boolean nodeMatched = false;

        if (nodeDTO != null && nodeDTO.getNodeHash() != null && nodeDTO.getNodeHash().equalsIgnoreCase(nodeHash)) {
            return true;
        }
        return nodeMatched;
    }

    private NodeDTO getParentNode(String nodeCode, String programCode, String hierarchyCode, String dataPartnerCode, String nodeHash) {

        String queryStr = "select node.id, node_code, node_name, parent_id, node_level,node_left_value, "
                + " node_right_value, hierarchy_id ,node_hash from ods.psh_product_hierarchy_node node , "
                + "ods.psh_product_hierarchy_catalog catalog where hierarchy_id = catalog.id and "
                + "node_code = :nodeCode and program_code = :programCode and hierarchy_code = :hierarchyCode "
                + "and datapartner_code = :dataPartnerCode and node_hash = :nodeHash";

        List<Object[]> nodeResultList = entityManager.createNativeQuery(queryStr).setParameter("nodeCode", nodeCode).
                setParameter(PROGRAM_CODE, programCode).setParameter(HIERARCHY_CODE, hierarchyCode).
                setParameter(DATA_PARTNER_CODE, dataPartnerCode).setParameter("nodeHash", nodeHash).getResultList();

        NodeDTO nodeDTO = null;
        for (int i = 0; i < nodeResultList.size(); i++) {
            Object[] nodeResult = nodeResultList.get(i);
            Object[] tuple = nodeResult;

            nodeDTO = buildNodeDTOFromResponse(tuple);
        }
        return nodeDTO;
    }

    @VisibleForTesting
    void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
