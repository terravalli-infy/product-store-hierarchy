package com.ga.endeavour.productstore.util;

import com.ga.endeavour.productstore.dto.NodeDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class ProductStoreHierarchyUtil {

    private static final double SEED = 9999999999999999999999999999999999.999999999999999999999999999999;
    private final BigDecimal bigSeed = BigDecimal.valueOf(SEED);
    private static final List<BigDecimal> PRIME_LIST = new ArrayList<>();
    private static ProductStoreHierarchyUtil productStoreHierarchyUtil = new ProductStoreHierarchyUtil();

    static {
        int limit = 1000000;
        for (int number = 2; number <= limit; number++) {
            if (productStoreHierarchyUtil.isPrime(number)) {
                PRIME_LIST.add(new BigDecimal(number));
            }
        }
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public NodeDTO calculateNodeValues(NodeDTO thisNodeDTO, NodeDTO parentNodeDTO, List<NodeDTO> nodeDTOList) {

        BigDecimal splitRange;
        if (parentNodeDTO != null && parentNodeDTO.getId() != null) {
            splitRange = parentNodeDTO.getNodeRightValue().subtract(parentNodeDTO.getNodeLeftValue()).abs();
        } else {
            //this is the root node
            thisNodeDTO.setNodeLeftValue(bigSeed);
            thisNodeDTO.setNodeRightValue(BigDecimal.ZERO);
            return thisNodeDTO;
        }

        int noOfChildren = 0;
        if (nodeDTOList != null && nodeDTOList.size() > 0) {
            noOfChildren = nodeDTOList.size();
        }
        BigDecimal leftDivisor = PRIME_LIST.get(noOfChildren);
        BigDecimal rightDivisor = PRIME_LIST.get(noOfChildren + 1);

        BigDecimal leftValue = splitRange.divide(leftDivisor, 30, RoundingMode.HALF_UP);
        leftValue = leftValue.add(parentNodeDTO.getNodeRightValue());

        BigDecimal rightValue = splitRange.divide(rightDivisor, 30, RoundingMode.HALF_UP);
        rightValue = rightValue.add(parentNodeDTO.getNodeRightValue());

        //check before setting left and right values, if the values are clashing with any other sibling, reassign
        if (nodeDTOList != null) {
            for (int i = 0; i < nodeDTOList.size(); i++) {
                NodeDTO nodeDTO = nodeDTOList.get(i);
                if (nodeDTO.getNodeLeftValue().equals(leftValue) && nodeDTO.getNodeRightValue().equals(rightValue) && !nodeDTO.getId().equals(thisNodeDTO.getId())) {
                    //re generate the left and right values
                    leftDivisor = PRIME_LIST.get(noOfChildren + i + 1);
                    rightDivisor = PRIME_LIST.get(noOfChildren + i + 2);
                }
                leftValue = splitRange.divide(leftDivisor, 30, RoundingMode.HALF_UP);
                leftValue = leftValue.add(parentNodeDTO.getNodeRightValue());

                rightValue = splitRange.divide(rightDivisor, 30, RoundingMode.HALF_UP);
                rightValue = rightValue.add(parentNodeDTO.getNodeRightValue());
            }
        }
        thisNodeDTO.setNodeLeftValue(leftValue);
        thisNodeDTO.setNodeRightValue(rightValue);
        return thisNodeDTO;
    }

}
Tirupathi Reddy Erravalli
