package com.ga.endeavour.productstore.service;

import com.ga.endeavour.constants.HierarchyConstants;
import com.ga.endeavour.platform.payload.hierarchy.request.HierarchyEntityRequest;
import com.ga.endeavour.platform.payload.hierarchy.request.HierarchyNodeRequest;
import com.ga.endeavour.platform.payload.hierarchy.request.HierarchyRequest;
import com.ga.endeavour.platform.payload.hierarchy.response.HierarchyEntityResponsePayload;
import com.ga.endeavour.platform.payload.hierarchy.response.HierarchyPathResponsePayload;
import com.ga.endeavour.productstore.assembler.ProductStoreHierarchyAssembler;
import com.ga.endeavour.productstore.domain.Hierarchy;
import com.ga.endeavour.productstore.domain.HierarchyEntity;
import com.ga.endeavour.productstore.domain.HierarchyNode;
import com.ga.endeavour.productstore.repository.ProductStoreHierarchyHibernateRepository;
import com.ga.endeavour.productstore.repository.ProductStoreHierarchyRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductStoreHierarchyServiceImpl implements ProductStoreHierarchyService {

    private final ProductStoreHierarchyHibernateRepository productStoreHierarchyHibernateRepository;
    private final ProductStoreHierarchyRepository productStoreHierarchyRepository;
    private final ProductStoreHierarchyAssembler productStoreHierarchyAssembler;

    @Autowired
    public ProductStoreHierarchyServiceImpl(
            ProductStoreHierarchyHibernateRepository productStoreHierarchyHibernateRepository,
            ProductStoreHierarchyRepository productStoreHierarchyRepository,
            ProductStoreHierarchyAssembler productStoreHierarchyAssembler) {
        this.productStoreHierarchyHibernateRepository = productStoreHierarchyHibernateRepository;
        this.productStoreHierarchyRepository = productStoreHierarchyRepository;
        this.productStoreHierarchyAssembler = productStoreHierarchyAssembler;
    }

    @Override
    public int createOrUpdateHierarchy(HierarchyRequest hierarchyRequest) {

        int result = productStoreHierarchyRepository.createOrUpdateHierarchy(
                hierarchyRequest.getProgramCode(),
                hierarchyRequest.getDataPartnerCode(), hierarchyRequest.getHierarchyType().name(),
                hierarchyRequest.getHierarchyName(), hierarchyRequest.getHierarchyName(),
                hierarchyRequest.getHierarchyCode());

        HierarchyNodeRequest hierarchyNodeRequest = populateRootNodeRequest(hierarchyRequest);
        createOrUpdateNode(hierarchyNodeRequest);

        return result;
    }

    private HierarchyNodeRequest populateRootNodeRequest(HierarchyRequest hierarchyRequest) {

        HierarchyNodeRequest hierarchyNodeRequest = new HierarchyNodeRequest();

        hierarchyNodeRequest.setProgramCode(hierarchyRequest.getProgramCode());
        hierarchyNodeRequest.setHierarchyCode(hierarchyRequest.getHierarchyCode());
        hierarchyNodeRequest.setDataPartnerCode(hierarchyRequest.getDataPartnerCode());
        hierarchyNodeRequest.setNodeName(hierarchyRequest.getHierarchyName());
        hierarchyNodeRequest.setNodeCode(hierarchyRequest.getHierarchyCode());
        hierarchyNodeRequest.setNodeHash(File.separator);
        hierarchyNodeRequest.setParentNodeCode("");
        hierarchyNodeRequest.setParentName("");

        return hierarchyNodeRequest;
    }

    @Override
    public int createOrUpdateNode(HierarchyNodeRequest hierarchyNodeRequest) {

        int result = productStoreHierarchyRepository.createOrUpdateNode(hierarchyNodeRequest);

        return result;
    }

    @Override
    public int createOrUpdateEntity(HierarchyEntityRequest hierarchyEntityRequest) {

        int result = productStoreHierarchyRepository.createOrUpdateEntity(hierarchyEntityRequest);

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
    public List<Hierarchy> getHierarchiesForProgram(String programCode, String dataPartnerCode, String hierarchyType) {

        List<Hierarchy> response = null;

        if (dataPartnerCode != null && StringUtils.isNotEmpty(dataPartnerCode)) {
            response = productStoreHierarchyHibernateRepository.findByProgramCodeDataPartnerCode(
                    programCode,
                    dataPartnerCode, hierarchyType);
        } else {
            response = productStoreHierarchyHibernateRepository.findByProgramCode(programCode, hierarchyType);
        }

        return response;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
    public List<HierarchyPathResponsePayload> getHierarchyPaths(
            String programCode, String hierarchyType, String
            hierarchyName, String nodeEntityType, String nodeEntityName, String dataPartnerCode) {

        List<HierarchyPathResponsePayload> responsePayloadList = new ArrayList<HierarchyPathResponsePayload>();

        if (nodeEntityType != null && nodeEntityType.equalsIgnoreCase(HierarchyConstants.NODE)) {

            List<HierarchyNode> hierarchyNodes = productStoreHierarchyHibernateRepository
                    .findIfNodeExists(programCode, hierarchyType, hierarchyName, nodeEntityName, dataPartnerCode);

            for (HierarchyNode hierarchyNode : hierarchyNodes) {
                if (hierarchyNode.getNodeLevel() == 0L) {
                    responsePayloadList.add(productStoreHierarchyAssembler
                            .hierarchyPathResponseAssemblerForNode(Long.toString(hierarchyNode.getId()), hierarchyNode.getNodeName()));
                    continue;
                }
                List<HierarchyNode> hierarchyNodeList = productStoreHierarchyHibernateRepository
                        .fetchParentHierarchyTreeForChildForNode(hierarchyNode.getHierarchy().getId(),
                                hierarchyNode.getNodeLeftValue(), hierarchyNode.getNodeRightValue());

                String nodeTreePath = formTreePath(hierarchyNodeList);

                if (hierarchyNodeList.size() > 0) {
                    responsePayloadList.add(
                            productStoreHierarchyAssembler.hierarchyPathResponseAssemblerForNode(Long.toString(hierarchyNode.getId()), nodeTreePath));
                }
            }
        } else if (nodeEntityType != null && nodeEntityType.equalsIgnoreCase(HierarchyConstants.ENTITY)) {

            List<HierarchyEntity> hierarchyEntities = productStoreHierarchyHibernateRepository
                    .findIfEntityExists(programCode, hierarchyType, hierarchyName, nodeEntityName, dataPartnerCode);

            for (HierarchyEntity hierarchyEntity : hierarchyEntities) {
                List<HierarchyNode> hierarchyNodeList = productStoreHierarchyHibernateRepository
                        .fetchParentHierarchyTreeForChildForEntity(
                                hierarchyEntity.getHierarchyNode().getHierarchy().getId(), hierarchyEntity
                                        .getHierarchyNode().getNodeLeftValue(), hierarchyEntity.getHierarchyNode().getNodeRightValue());

                String nodeTreePath = formTreePath(hierarchyNodeList);

                if (hierarchyNodeList.size() > 0) {
                    responsePayloadList.add(
                            productStoreHierarchyAssembler
                                    .hierarchyPathResponseAssemblerForEntity(hierarchyEntity, nodeTreePath));
                }
            }
        }

        return responsePayloadList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
    public HierarchyEntityResponsePayload getHierarchyEntities(String nodeOrEntity, String nodeEntityId) {
        String entityId = null;
        String parentNodeId = null;
        HierarchyEntityResponsePayload responsePayload = new HierarchyEntityResponsePayload(new ArrayList<String>());

        if (nodeOrEntity != null && nodeOrEntity.equalsIgnoreCase(HierarchyConstants.NODE)) {
            HierarchyNode hierarchyNode = productStoreHierarchyHibernateRepository.fetchNodeFromNodeId(
                    Long.valueOf(nodeEntityId).longValue());

            if (hierarchyNode != null) {
                List<HierarchyNode> hierarchyChildNodes = productStoreHierarchyHibernateRepository
                        .fetchChildNodesForNode(hierarchyNode.getHierarchy().getId(), hierarchyNode.getNodeLeftValue(),
                                hierarchyNode.getNodeRightValue());

                responsePayload = productStoreHierarchyAssembler.hierarchyEntityResponseAssembler(hierarchyChildNodes);

                for (HierarchyEntity entity : hierarchyNode.getEntities()) {
                    responsePayload.getEntitySkuCodes().add(entity.getEntityCode().toUpperCase());
                }

            }
        } else if (nodeOrEntity != null && nodeOrEntity.equalsIgnoreCase(HierarchyConstants.ENTITY)) {

            String[] ids = nodeEntityId.split(":");

            if (ids.length == 2) {
                entityId = ids[0];
                parentNodeId = ids[1];

                HierarchyEntity hierarchyEntity = productStoreHierarchyHibernateRepository.
                        fetchEntityFromEntityId(Long.valueOf(entityId).longValue(), Long.valueOf(parentNodeId).longValue());

                if (hierarchyEntity != null) {
                    List<String> entitySkuCodes = new ArrayList<String>();
                    entitySkuCodes.add(hierarchyEntity.getEntityCode().toUpperCase());

                    if (entitySkuCodes.size() > 0) {
                        responsePayload = new HierarchyEntityResponsePayload(entitySkuCodes);
                    }
                }
            }
        }
        return responsePayload;
    }

    public String formTreePath(List<HierarchyNode> hierarchyNodeList) {
        StringBuffer nodeTreePath = new StringBuffer();
        nodeTreePath.append("");

        for (HierarchyNode hierarchyNodeTree : hierarchyNodeList) {
            nodeTreePath = nodeTreePath.append(File.separator).append(hierarchyNodeTree.getNodeName().toUpperCase());
        }
        return nodeTreePath.toString();
    }
}
